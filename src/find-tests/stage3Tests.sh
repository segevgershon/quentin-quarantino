#!/bin/bash

let failedTests=0
testsFolder=/tmp/find${RANDOM}
symlincFolder=/tmp/find${RANDOM}
findResultFile=/tmp/findResult
customeFindResultFile=/tmp/customeFindResult

# Change it according to your find program
customeFindPath=find

function PrintError
{
    echo "----------------------"
    echo "fail on test $1"
    echo "tests cmd $2"
    echo "find results are"
    echo $3
    echo "custome find results are"
    echo "find results are"
    echo $4
    let failedTests=$failedTests+1
    echo "----------------------"
}

function RunTest
{
    cmd=$1
    functionName=$2
    findResult=`find $cmd > $findResultFile`
    customeFindResult=`$customeFindPath $cmd > $customeFindResultFile`
    
    if [[ $customeFindResult -ne $findResult ]]
    then
        PrintError $functionName $cmd $findResult $customeFindResult
    fi

    if [[ $customeFindResult -ne 0 ]]
    then
        # if both fail it should not print the same error massage 
        return 0
    fi

    if [[ `diff $findResultFile $customeFindResultFile | wc -l` -gt 0 ]]
    then
        PrintError $functionName `cat $findResultFile` `cat $customeFindResultFile`
    fi
}


function MakeTestsFiles
{
    mkdir $testsFolder
    mkdir $symlincFolder

    cd $testsFolder
    mkdir -p a/b/c/
    mkdir /tmp/$
    touch $symlincFolder/symlineFile
    ln -s $symlincFolder
}

function RemoveTestFiles
{
    rm -rf $testsFolder
    rm $customeFindResultFile $findResultFile
    rm -rf $symlincFolder
}

function BadParams
{
    RunTest "-L 7 -name a" BadParams
}

function SearchWithoutSymlink
{
    RunTest "-name symlineFile" SearchWithoutSymlink
}

function SearchWithSymlink
{
    RunTest "-L -name symlineFile" SearchWithSymlink
}

MakeTestsFiles
BadParams
SearchWithoutSymlink
SearchWithSymlink
if [[ $failedTests -eq 0 ]]
then
    echo "all tests passed :)"
else
    echo "failed in $failedTests tests"
fi
RemoveTestFiles